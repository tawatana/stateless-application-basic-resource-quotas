# Stateless_Application_Basic_resource_quotas
検証コードもろもろ

# メモ
## 基本
limits.cpu,mem,requests.cpu,menがresource_quotasとして指定されていたとき
基本limitを指定してあげればよい。requestsは指定してあげないのならば0になる。  
逆にrequestsのみだとエラーになる。limitがは指定されていないと上限まで確保しようとする  
暗黙的にネームスペースの総リソースを限定するのではなくて、podに指定してあげたlimits,requestsを制限して実現する。  

## Quota Scopesについて
https://kubernetes.io/docs/concepts/policy/resource-quotas/#quota-scopes
